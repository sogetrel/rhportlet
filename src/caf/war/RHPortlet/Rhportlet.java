/**
 * 
 */
package caf.war.RHPortlet;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * @author vital.thyot
 *
 */
@ManagedBean(name = "Rhportlet")
@ApplicationScoped
@DTManagedBean(displayName = "RHPortlet", beanType = BeanType.APPLICATION)
public class Rhportlet extends com.webmethods.caf.faces.bean.BaseApplicationBean 
{
	private transient caf.war.RHPortlet.wsclient.sogecockpitservice.wsd.wscockpit.LireInstancesFluxDunFlux lireInstancesFluxDunFlux = null;
	private static final String[][] LIREINSTANCESFLUXDUNFLUX_PROPERTY_BINDINGS = new String[][] {
		{"#{LireInstancesFluxDunFlux.authCredentials.authenticationMethod}", "1"},
		{"#{LireInstancesFluxDunFlux.authCredentials.requiresAuth}", "true"},
		{"#{LireInstancesFluxDunFlux.autoRefresh}", "false"},
	};
	public Rhportlet()
	{
		super();
		setCategoryName( "CafApplication" );
		setSubCategoryName( "RHPortlet" );
	}
	public caf.war.RHPortlet.wsclient.sogecockpitservice.wsd.wscockpit.LireInstancesFluxDunFlux getLireInstancesFluxDunFlux()  {
		if (lireInstancesFluxDunFlux == null) {
		    lireInstancesFluxDunFlux = (caf.war.RHPortlet.wsclient.sogecockpitservice.wsd.wscockpit.LireInstancesFluxDunFlux)resolveExpression("#{LireInstancesFluxDunFlux}");
		}
	
	    resolveDataBinding(LIREINSTANCESFLUXDUNFLUX_PROPERTY_BINDINGS, lireInstancesFluxDunFlux, "lireInstancesFluxDunFlux", false, false);
		return lireInstancesFluxDunFlux;
	}


}