/**
 * 
 */
package caf.war.RHPortlet.gestionnotilus;

/**
 * @author vital.thyot
 *
 */

import javax.portlet.PortletPreferences;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

@ManagedBean(name = "GestionNotilus")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(displayName = "GestionNotilus", beanType = BeanType.PORTLET)
public class GestionNotilus  extends   com.webmethods.caf.faces.bean.BaseFacesPreferencesBean {

	public static final String[] PREFERENCES_NAMES = new String[] {};
	private transient caf.war.RHPortlet.Rhportlet rhportlet = null;
	
	/**
	 * Create new preferences bean with list of preference names
	 */
	public GestionNotilus() {
		super(PREFERENCES_NAMES);
	}
	
	/**
	 * Call this method in order to persist
	 * Portlet preferences
	 */
	public void storePreferences() throws Exception {
		updatePreferences();
		PortletPreferences preferences = getPreferences();
		preferences.store();
	}

	public caf.war.RHPortlet.Rhportlet getRhportlet()  {
		if (rhportlet == null) {
		    rhportlet = (caf.war.RHPortlet.Rhportlet)resolveExpression("#{Rhportlet}");
		}
		return rhportlet;
	}
}