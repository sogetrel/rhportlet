/**
 * 
 */
package caf.war.RHPortlet.gestionnotilus;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * @author vital.thyot
 *
 */

@ManagedBean(name = "GestionNotilusDefaultviewView")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(displayName = "GestionNotilus/default", beanType = BeanType.PAGE)
public class GestionNotilusDefaultviewView  extends   com.webmethods.caf.faces.bean.BasePageBean {

	/**
	 * Determines if a de-serialized file is compatible with this class.
	 *
	 * Maintainers must change this value if and only if the new version
	 * of this class is not compatible with old versions. See Sun docs
	 * for <a href=http://java.sun.com/j2se/1.5.0/docs/guide/serialization/spec/version.html> 
	 * details. </a>
	 */
	private static final long serialVersionUID = 1L;
	private transient caf.war.RHPortlet.gestionnotilus.GestionNotilus gestionNotilus = null;
	private transient caf.war.RHPortlet.wsclient.sogerepasnotilusin.ws.wsinterfacenotilus.SendRepas sendRepas = null;
	private java.lang.Boolean displayInstance;
	private static final String[][] INITIALIZE_PROPERTY_BINDINGS = new String[][] {
		{"#{GestionNotilusDefaultviewView.displayInstance}", "false"},
	};
	private static final String[][] ONLANCER_PROPERTY_BINDINGS = new String[][] {
		{"#{GestionNotilusDefaultviewView.sendRepas.refresh}", null},
		{"#{GestionNotilusDefaultviewView.displayInstance}", "true"},
		{"#{GestionNotilusDefaultviewView.lireInstancesFluxDunFlux.parameters.lireInstancesFluxDunFlux.lireInstancesFluxDunFlux.IDDemiFluxIn}", "31"},
		{"#{GestionNotilusDefaultviewView.lireInstancesFluxDunFlux.parameters.lireInstancesFluxDunFlux.lireInstancesFluxDunFlux.IDDemiFluxOut}", "32"},
		{"#{GestionNotilusDefaultviewView.lireInstancesFluxDunFlux.refresh}", null},
	};
	private static final String[][] SENDREPAS_PROPERTY_BINDINGS = new String[][] {
		{"#{sendRepas.authCredentials.authenticationMethod}", "1"},
		{"#{sendRepas.authCredentials.requiresAuth}", "true"},
		{"#{sendRepas.autoRefresh}", "false"},
		{"#{sendRepas.endpointAddress}", "#{environment[\"wsclient-endpointAddress-Notilus\"]}"},
	};
	private transient caf.war.RHPortlet.wsclient.sogecockpitservice.wsd.wscockpit.LireInstancesFluxDunFlux lireInstancesFluxDunFlux = null;
	private static final String[][] LIREINSTANCESFLUXDUNFLUX_PROPERTY_BINDINGS = new String[][] {
		{"#{lireInstancesFluxDunFlux.authCredentials.authenticationMethod}", "1"},
		{"#{lireInstancesFluxDunFlux.authCredentials.requiresAuth}", "true"},
		{"#{lireInstancesFluxDunFlux.autoRefresh}", "false"},
		{"#{lireInstancesFluxDunFlux.endpointAddress}", "#{environment[\"wsclient-endpointAddress-Cockpit\"]}"},
	};
	/**
	 * Initialize page
	 */
	public String initialize() {
		try {
		    resolveDataBinding(INITIALIZE_PROPERTY_BINDINGS, null, "initialize", true, false);
		} catch (Exception e) {
			error(e);
			log(e);
		}
		return null;	
	}

	public caf.war.RHPortlet.gestionnotilus.GestionNotilus getGestionNotilus()  {
		if (gestionNotilus == null) {
		    gestionNotilus = (caf.war.RHPortlet.gestionnotilus.GestionNotilus)resolveExpression("#{GestionNotilus}");
		}
		return gestionNotilus;
	}

	public caf.war.RHPortlet.wsclient.sogerepasnotilusin.ws.wsinterfacenotilus.SendRepas getSendRepas()  {
		if (sendRepas == null) {
		    sendRepas = (caf.war.RHPortlet.wsclient.sogerepasnotilusin.ws.wsinterfacenotilus.SendRepas)resolveExpression("#{SendRepas}");
		}
	
	    resolveDataBinding(SENDREPAS_PROPERTY_BINDINGS, sendRepas, "sendRepas", false, false);
		return sendRepas;
	}

	public String onLancer() {
	    resolveDataBinding(ONLANCER_PROPERTY_BINDINGS, this, "onLancer.this", true, false);
		return null;
	}

	public java.lang.Boolean getDisplayInstance()  {
		
		return displayInstance;
	}

	public void setDisplayInstance(java.lang.Boolean displayInstance)  {
		this.displayInstance = displayInstance;
	}

	public caf.war.RHPortlet.wsclient.sogecockpitservice.wsd.wscockpit.LireInstancesFluxDunFlux getLireInstancesFluxDunFlux()  {
		if (lireInstancesFluxDunFlux == null) {
		    lireInstancesFluxDunFlux = (caf.war.RHPortlet.wsclient.sogecockpitservice.wsd.wscockpit.LireInstancesFluxDunFlux)resolveExpression("#{LireInstancesFluxDunFlux}");
		}
	
	    resolveDataBinding(LIREINSTANCESFLUXDUNFLUX_PROPERTY_BINDINGS, lireInstancesFluxDunFlux, "lireInstancesFluxDunFlux", false, false);
		return lireInstancesFluxDunFlux;
	}
	
}