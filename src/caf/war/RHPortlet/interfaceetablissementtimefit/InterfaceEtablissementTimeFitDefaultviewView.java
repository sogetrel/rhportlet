/**
 * 
 */
package caf.war.RHPortlet.interfaceetablissementtimefit;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * @author vital.thyot
 *
 */

@ManagedBean(name = "InterfaceEtablissementTimeFitDefaultviewView")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(displayName = "InterfaceEtablissementTimeFit/default", beanType = BeanType.PAGE)
public class InterfaceEtablissementTimeFitDefaultviewView  extends   com.webmethods.caf.faces.bean.BasePageBean {

	/**
	 * Determines if a de-serialized file is compatible with this class.
	 *
	 * Maintainers must change this value if and only if the new version
	 * of this class is not compatible with old versions. See Sun docs
	 * for <a href=http://java.sun.com/j2se/1.5.0/docs/guide/serialization/spec/version.html> 
	 * details. </a>
	 */
	private static final long serialVersionUID = 1L;
	private transient caf.war.RHPortlet.interfaceetablissementtimefit.InterfaceEtablissementTimeFit interfaceEtablissementTimeFit = null;
	private transient caf.war.RHPortlet.wsclient.sogeetablissementsaphyrin.wsd.wsinterfaceetablissement.SendEtablissement sendEtablissement = null;
	private java.lang.Boolean displayInstance;
	private static final String[][] INITIALIZE_PROPERTY_BINDINGS = new String[][] {
		{"#{InterfaceEtablissementTimeFitDefaultviewView.displayInstance}", "false"},
	};
	private static final String[][] SENDETABLISSEMENT_PROPERTY_BINDINGS = new String[][] {
		{"#{sendEtablissement.authCredentials.authenticationMethod}", "1"},
		{"#{sendEtablissement.authCredentials.requiresAuth}", "true"},
		{"#{sendEtablissement.autoRefresh}", "false"},
		{"#{sendEtablissement.endpointAddress}", "#{environment[\"wsclient-endpointAddress-Etablissement\"]}"},
	};
	private static final String[][] ONLANCER_PROPERTY_BINDINGS = new String[][] {
		{"#{InterfaceEtablissementTimeFitDefaultviewView.sendEtablissement.refresh}", null},
		{"#{InterfaceEtablissementTimeFitDefaultviewView.lireInstancesFluxDunFlux.parameters.lireInstancesFluxDunFlux.lireInstancesFluxDunFlux.IDDemiFluxIn}", "5"},
		{"#{InterfaceEtablissementTimeFitDefaultviewView.lireInstancesFluxDunFlux.parameters.lireInstancesFluxDunFlux.lireInstancesFluxDunFlux.IDDemiFluxOut}", "6"},
		{"#{InterfaceEtablissementTimeFitDefaultviewView.lireInstancesFluxDunFlux.refresh}", null},
		{"#{InterfaceEtablissementTimeFitDefaultviewView.displayInstance}", "true"},
	};
	private transient caf.war.RHPortlet.wsclient.sogecockpitservice.wsd.wscockpit.LireInstancesFluxDunFlux lireInstancesFluxDunFlux = null;
	private static final String[][] LIREINSTANCESFLUXDUNFLUX_PROPERTY_BINDINGS = new String[][] {
		{"#{lireInstancesFluxDunFlux.authCredentials.authenticationMethod}", "1"},
		{"#{lireInstancesFluxDunFlux.authCredentials.requiresAuth}", "true"},
		{"#{lireInstancesFluxDunFlux.autoRefresh}", "false"},
		{"#{lireInstancesFluxDunFlux.endpointAddress}", "#{environment[\"wsclient-endpointAddress-Cockpit\"]}"},
	};
	/**
	 * Initialize page
	 */
	public String initialize() {
		try {
		    resolveDataBinding(INITIALIZE_PROPERTY_BINDINGS, null, "initialize", true, false);
		} catch (Exception e) {
			error(e);
			log(e);
		}
		return null;	
	}

	public caf.war.RHPortlet.interfaceetablissementtimefit.InterfaceEtablissementTimeFit getInterfaceEtablissementTimeFit()  {
		if (interfaceEtablissementTimeFit == null) {
		    interfaceEtablissementTimeFit = (caf.war.RHPortlet.interfaceetablissementtimefit.InterfaceEtablissementTimeFit)resolveExpression("#{InterfaceEtablissementTimeFit}");
		}
		return interfaceEtablissementTimeFit;
	}

	public caf.war.RHPortlet.wsclient.sogeetablissementsaphyrin.wsd.wsinterfaceetablissement.SendEtablissement getSendEtablissement()  {
		if (sendEtablissement == null) {
		    sendEtablissement = (caf.war.RHPortlet.wsclient.sogeetablissementsaphyrin.wsd.wsinterfaceetablissement.SendEtablissement)resolveExpression("#{SendEtablissement}");
		}
	
	    resolveDataBinding(SENDETABLISSEMENT_PROPERTY_BINDINGS, sendEtablissement, "sendEtablissement", false, false);
		return sendEtablissement;
	}

	public String onLancer() {
	    resolveDataBinding(ONLANCER_PROPERTY_BINDINGS, this, "onLancer.this", true, false);
		return null;
	}

	public java.lang.Boolean getDisplayInstance()  {
		
		return displayInstance;
	}

	public void setDisplayInstance(java.lang.Boolean displayInstance)  {
		this.displayInstance = displayInstance;
	}

	public caf.war.RHPortlet.wsclient.sogecockpitservice.wsd.wscockpit.LireInstancesFluxDunFlux getLireInstancesFluxDunFlux()  {
		if (lireInstancesFluxDunFlux == null) {
		    lireInstancesFluxDunFlux = (caf.war.RHPortlet.wsclient.sogecockpitservice.wsd.wscockpit.LireInstancesFluxDunFlux)resolveExpression("#{LireInstancesFluxDunFlux}");
		}
	
	    resolveDataBinding(LIREINSTANCESFLUXDUNFLUX_PROPERTY_BINDINGS, lireInstancesFluxDunFlux, "lireInstancesFluxDunFlux", false, false);
		return lireInstancesFluxDunFlux;
	}
	
}