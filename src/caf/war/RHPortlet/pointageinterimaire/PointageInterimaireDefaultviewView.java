/**
 * 
 */
package caf.war.RHPortlet.pointageinterimaire;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * @author vital.thyot
 *
 */

@ManagedBean(name = "PointageInterimaireDefaultviewView")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(displayName = "PointageInterimaire/default", beanType = BeanType.PAGE)
public class PointageInterimaireDefaultviewView  extends   com.webmethods.caf.faces.bean.BasePageBean {

	/**
	 * Determines if a de-serialized file is compatible with this class.
	 *
	 * Maintainers must change this value if and only if the new version
	 * of this class is not compatible with old versions. See Sun docs
	 * for <a href=http://java.sun.com/j2se/1.5.0/docs/guide/serialization/spec/version.html> 
	 * details. </a>
	 */
	private static final long serialVersionUID = 1L;
	private static final String[][] LANCERAGENT_PROPERTY_BINDINGS = new String[][] {
		{"#{lancerAgent.authCredentials.authenticationMethod}", "1"},
		{"#{lancerAgent.authCredentials.requiresAuth}", "true"},
		{"#{lancerAgent.endpointAddress}", "#{environment[\"wsclient-endpointAddress-ServeurBI\"]}"},
	};
	private static final String[][] LIREDERNIEREINSTANCEAGENT_PROPERTY_BINDINGS = new String[][] {
		{"#{lireDerniereInstanceAgent.authCredentials.authenticationMethod}", "1"},
		{"#{lireDerniereInstanceAgent.authCredentials.requiresAuth}", "true"},
		{"#{lireDerniereInstanceAgent.endpointAddress}", "#{environment[\"wsclient-endpointAddress-ServeurBI\"]}"},
	};
	private static final String[][] VERIFIERJOBENCOURS_PROPERTY_BINDINGS = new String[][] {
		{"#{verifierJobEnCours.authCredentials.authenticationMethod}", "1"},
		{"#{verifierJobEnCours.authCredentials.requiresAuth}", "true"},
		{"#{verifierJobEnCours.endpointAddress}", "#{environment[\"wsclient-endpointAddress-ServeurBI\"]}"},
	};
	private transient caf.war.RHPortlet.pointageinterimaire.PointageInterimaire pointageInterimaire = null;

	private transient caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.LancerAgent lancerAgent = null;
	private transient caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.LireDerniereInstanceAgent lireDerniereInstanceAgent = null;
	private transient caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.VerifierJobEnCours verifierJobEnCours = null;
	private java.lang.Boolean displayFire;
	private java.lang.Boolean displayPnl;
	private java.lang.String urlFire;
	private static final String[][] ONCHANGEURL_PROPERTY_BINDINGS = new String[][] {
	};
	private static final String[][] ONGIVEETATHISTORIQUE_PROPERTY_BINDINGS = new String[][] {
	};
	private static final String[][] INITIALIZE_PROPERTY_BINDINGS = new String[][] {
		{"#{PointageInterimaireDefaultviewView.urlFire}", "/pictures/feuRouge.png"},
		{"#{PointageInterimaireDefaultviewView.displayFire}", "false"},
		{"#{PointageInterimaireDefaultviewView.displayPnl}", "false"},
	};
	private java.lang.String isRunning;
	private static final String[][] ONCHECKAVAILIBILITY_PROPERTY_BINDINGS = new String[][] {
		{"#{PointageInterimaireDefaultviewView.displayFire}", "true"},
		{"#{PointageInterimaireDefaultviewView.verifierJobEnCours.parameters.verifierJobEnCours.verifierJobEnCours.name}", "PointageInterimaire"},
		{"#{PointageInterimaireDefaultviewView.verifierJobEnCours.refresh}", null},
		{"#{PointageInterimaireDefaultviewView.isRunning}", "#{PointageInterimaireDefaultviewView.verifierJobEnCours.result.verifierJobEnCoursResponse.isRunning}"},
		{"#{PointageInterimaireDefaultviewView.onChangeUrl}", null},
	};
	private java.lang.String etatHistorique;
	private static final String[][] ONLAUNCH_PROPERTY_BINDINGS = new String[][] {
		{"#{PointageInterimaireDefaultviewView.lancerAgent.parameters.lancerAgent.lancerAgent.name}", "PointageInterimaire"},
		{"#{PointageInterimaireDefaultviewView.lancerAgent.refresh}", null},
		{"#{PointageInterimaireDefaultviewView.displayPnl}", "true"},
		{"#{PointageInterimaireDefaultviewView.lireDerniereInstanceAgent.parameters.lireDerniereInstanceAgent.lireDerniereInstanceAgent.name}", "PointageInterimaire"},
		{"#{PointageInterimaireDefaultviewView.lireDerniereInstanceAgent.refresh}", null},
		{"#{PointageInterimaireDefaultviewView.onGiveEtatHistorique}", null},
		{"#{PointageInterimaireDefaultviewView.onCheckAvailibility}", null},
	};
	/**
	 * Initialize page
	 */
	public String initialize() {
		try {
		    resolveDataBinding(INITIALIZE_PROPERTY_BINDINGS, null, "initialize", true, false);
		} catch (Exception e) {
			error(e);
			log(e);
		}
		return null;	
	}

	public caf.war.RHPortlet.pointageinterimaire.PointageInterimaire getPointageInterimaire()  {
		if (pointageInterimaire == null) {
		    pointageInterimaire = (caf.war.RHPortlet.pointageinterimaire.PointageInterimaire)resolveExpression("#{PointageInterimaire}");
		}
		return pointageInterimaire;
	}
	
	public caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.LancerAgent getLancerAgent()  {
		if (lancerAgent == null) {
		    lancerAgent = (caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.LancerAgent)resolveExpression("#{LancerAgent}");
		}
	
	    resolveDataBinding(LANCERAGENT_PROPERTY_BINDINGS, lancerAgent, "lancerAgent", false, false);
		return lancerAgent;
	}

	public caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.LireDerniereInstanceAgent getLireDerniereInstanceAgent()  {
		if (lireDerniereInstanceAgent == null) {
		    lireDerniereInstanceAgent = (caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.LireDerniereInstanceAgent)resolveExpression("#{LireDerniereInstanceAgent}");
		}
	
	    resolveDataBinding(LIREDERNIEREINSTANCEAGENT_PROPERTY_BINDINGS, lireDerniereInstanceAgent, "lireDerniereInstanceAgent", false, false);
		return lireDerniereInstanceAgent;
	}

	public caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.VerifierJobEnCours getVerifierJobEnCours()  {
		if (verifierJobEnCours == null) {
		    verifierJobEnCours = (caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.VerifierJobEnCours)resolveExpression("#{VerifierJobEnCours}");
		}
	
	    resolveDataBinding(VERIFIERJOBENCOURS_PROPERTY_BINDINGS, verifierJobEnCours, "verifierJobEnCours", false, false);
		return verifierJobEnCours;
	}

	public java.lang.Boolean getDisplayFire()  {
		
		return displayFire;
	}

	public void setDisplayFire(java.lang.Boolean displayFire)  {
		this.displayFire = displayFire;
	}

	public java.lang.Boolean getDisplayPnl()  {
		
		return displayPnl;
	}

	public void setDisplayPnl(java.lang.Boolean displayPnl)  {
		this.displayPnl = displayPnl;
	}

	public java.lang.String getUrlFire()  {
		
		return urlFire;
	}

	public void setUrlFire(java.lang.String urlFire)  {
		this.urlFire = urlFire;
	}

	public String onCheckAvailibility() {
	    resolveDataBinding(ONCHECKAVAILIBILITY_PROPERTY_BINDINGS, this, "onCheckAvailibility.this", true, false);
		return null;
	}

	public java.lang.String getIsRunning()  {
		
		return isRunning;
	}

	public void setIsRunning(java.lang.String isRunning)  {
		this.isRunning = isRunning;
	}
	
	public String onChangeUrl() {
	    resolveDataBinding(ONCHANGEURL_PROPERTY_BINDINGS, this, "onChangeUrl.this", true, false);
	    if (this.getIsRunning().trim().toUpperCase().equals("OUI")){
			this.setUrlFire("/pictures/feuRouge.png");
		} else if (this.getIsRunning().trim().toUpperCase().equals("NON")) {
			this.setUrlFire("/pictures/feuVert.png");
		}
	    return null;
	}
	
	public String onGiveEtatHistorique() {
	    resolveDataBinding(ONGIVEETATHISTORIQUE_PROPERTY_BINDINGS, this, "onGiveEtatHistorique.this", true, false);
		if (!this.lireDerniereInstanceAgent.getResult().getLireDerniereInstanceAgentResponse().getHisto().getStepId().equals("0")){
			this.setEtatHistorique("En cours");
		} else {
		    if (this.lireDerniereInstanceAgent.getResult().getLireDerniereInstanceAgentResponse().getHisto().getRunStatus().equals("0")){
				this.setEtatHistorique("Echec");
		    } else if (this.lireDerniereInstanceAgent.getResult().getLireDerniereInstanceAgentResponse().getHisto().getRunStatus().equals("1")){
				this.setEtatHistorique("Succ�s");
		    } else if (this.lireDerniereInstanceAgent.getResult().getLireDerniereInstanceAgentResponse().getHisto().getRunStatus().equals("2")){
				this.setEtatHistorique("R�essaie");
		    } else if (this.lireDerniereInstanceAgent.getResult().getLireDerniereInstanceAgentResponse().getHisto().getRunStatus().equals("3")){
				this.setEtatHistorique("Abandonn�");
			}
		}
	    
	    return null;
	}

	public String onLaunch() {
	    resolveDataBinding(ONLAUNCH_PROPERTY_BINDINGS, this, "onLaunch.this", true, false);
		return null;
	}

	public java.lang.String getEtatHistorique()  {
		
		return etatHistorique;
	}

	public void setEtatHistorique(java.lang.String etatHistorique)  {
		this.etatHistorique = etatHistorique;
	}
	
}