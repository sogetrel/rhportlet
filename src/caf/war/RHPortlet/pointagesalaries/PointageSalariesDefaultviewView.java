/**
 * 
 */
package caf.war.RHPortlet.pointagesalaries;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * @author vital.thyot
 *
 */

@ManagedBean(name = "PointageSalariesDefaultviewView")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(displayName = "PointageSalaries/default", beanType = BeanType.PAGE)
public class PointageSalariesDefaultviewView  extends   com.webmethods.caf.faces.bean.BasePageBean {

	/**
	 * Determines if a de-serialized file is compatible with this class.
	 *
	 * Maintainers must change this value if and only if the new version
	 * of this class is not compatible with old versions. See Sun docs
	 * for <a href=http://java.sun.com/j2se/1.5.0/docs/guide/serialization/spec/version.html> 
	 * details. </a>
	 */
	private static final long serialVersionUID = 1L;
	private transient caf.war.RHPortlet.pointagesalaries.PointageSalaries pointageSalaries = null;
	private java.lang.String company;
	private java.lang.Boolean displayPnl;
	private static final String[][] ONSETVARIABLESTONULL_PROPERTY_BINDINGS = new String[][] {
	};
	private java.lang.Boolean displayFire;
	private java.lang.String urlFire;
	private static final String[][] ONCHANGEURL_PROPERTY_BINDINGS = new String[][] {
	};
	private java.lang.String isRunning;
	private java.lang.String etatHistorique;
	private static final String[][] GETETATHISTORIQUE_PROPERTY_BINDINGS = new String[][] {
	};
	private static final String[][] ONGIVEETATHISTORIQUE_PROPERTY_BINDINGS = new String[][] {
	};
	private static final String[][] ONREFRESHHISTORIQUE_PROPERTY_BINDINGS = new String[][] {
		{"#{PointageSalariesDefaultviewView.lireDerniereInstanceAgent.refresh}", null},
		{"#{PointageSalariesDefaultviewView.onGiveEtatHistorique}", null},
		{"#{PointageSalariesDefaultviewView.onCheckAvailability}", null},
	};
	private transient caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsbi.ListeSocieteTimeFit listeSocieteTimeFit = null;
	private static final String[][] LISTESOCIETETIMEFIT_PROPERTY_BINDINGS = new String[][] {
		{"#{listeSocieteTimeFit.authCredentials.authenticationMethod}", "1"},
		{"#{listeSocieteTimeFit.authCredentials.requiresAuth}", "true"},
		{"#{listeSocieteTimeFit.autoRefresh}", "false"},
		{"#{listeSocieteTimeFit.endpointAddress}", "#{environment[\"wsclient-endpointAddress-BI\"]}"},
	};
	private static final String[][] INITIALIZE_PROPERTY_BINDINGS = new String[][] {
		{"#{PointageSalariesDefaultviewView.listeSocieteTimeFit.refresh}", null},
		{"#{PointageSalariesDefaultviewView.displayPnl}", "false"},
		{"#{PointageSalariesDefaultviewView.displayFire}", "false"},
		{"#{PointageSalariesDefaultviewView.urlFire}", "/pictures/feuRouge.png"},
	};
	private transient caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.AjoutVariableToVariablesEnvironnement ajoutVariableToVariablesEnvironnement = null;
	private transient caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.DeleteAllVariableEnvironment deleteAllVariableEnvironment = null;
	private transient caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.LancerAgent lancerAgent = null;
	private transient caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.LireDerniereInstanceAgent lireDerniereInstanceAgent = null;
	private transient caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.VerifierJobEnCours verifierJobEnCours = null;
	private static final String[][] AJOUTVARIABLETOVARIABLESENVIRONNEMENT_PROPERTY_BINDINGS = new String[][] {
		{"#{ajoutVariableToVariablesEnvironnement.authCredentials.authenticationMethod}", "1"},
		{"#{ajoutVariableToVariablesEnvironnement.authCredentials.requiresAuth}", "true"},
		{"#{ajoutVariableToVariablesEnvironnement.autoRefresh}", "false"},
		{"#{ajoutVariableToVariablesEnvironnement.endpointAddress}", "#{environment[\"wsclient-endpointAddress-ServeurBI\"]}"},
	};
	private static final String[][] DELETEALLVARIABLEENVIRONMENT_PROPERTY_BINDINGS = new String[][] {
		{"#{deleteAllVariableEnvironment.authCredentials.authenticationMethod}", "1"},
		{"#{deleteAllVariableEnvironment.authCredentials.requiresAuth}", "true"},
		{"#{deleteAllVariableEnvironment.endpointAddress}", "#{environment[\"wsclient-endpointAddress-ServeurBI\"]}"},
	};
	private static final String[][] LANCERAGENT_PROPERTY_BINDINGS = new String[][] {
		{"#{lancerAgent.authCredentials.authenticationMethod}", "1"},
		{"#{lancerAgent.authCredentials.requiresAuth}", "true"},
		{"#{lancerAgent.endpointAddress}", "#{environment[\"wsclient-endpointAddress-ServeurBI\"]}"},
	};
	private static final String[][] LIREDERNIEREINSTANCEAGENT_PROPERTY_BINDINGS = new String[][] {
		{"#{lireDerniereInstanceAgent.authCredentials.authenticationMethod}", "1"},
		{"#{lireDerniereInstanceAgent.authCredentials.requiresAuth}", "true"},
		{"#{lireDerniereInstanceAgent.endpointAddress}", "#{environment[\"wsclient-endpointAddress-ServeurBI\"]}"},
	};
	private static final String[][] VERIFIERJOBENCOURS_PROPERTY_BINDINGS = new String[][] {
		{"#{verifierJobEnCours.authCredentials.authenticationMethod}", "1"},
		{"#{verifierJobEnCours.authCredentials.requiresAuth}", "true"},
		{"#{verifierJobEnCours.endpointAddress}", "#{environment[\"wsclient-endpointAddress-ServeurBI\"]}"},
	};
	private static final String[][] ONCHECKAVAILABILITY_PROPERTY_BINDINGS = new String[][] {
		{"#{PointageSalariesDefaultviewView.displayFire}", "true"},
		{"#{PointageSalariesDefaultviewView.verifierJobEnCours.parameters.verifierJobEnCours.verifierJobEnCours.name}", "PointageGTA"},
		{"#{PointageSalariesDefaultviewView.verifierJobEnCours.refresh}", null},
		{"#{PointageSalariesDefaultviewView.isRunning}", "#{PointageSalariesDefaultviewView.verifierJobEnCours.result.verifierJobEnCoursResponse.isRunning}"},
		{"#{PointageSalariesDefaultviewView.onChangeUrl}", null},
	};
	private static final String[][] ONSELECTCOMPANY_PROPERTY_BINDINGS = new String[][] {
		{"#{PointageSalariesDefaultviewView.deleteAllVariableEnvironment.parameters.deleteAllVariableEnvironment.deleteAllVariableEnvironment.variablesEnvironnement}", "#{PointageSalariesDefaultviewView.lancerAgent.parameters.lancerAgent.lancerAgent.variablesEnvironnement}"},
		{"#{PointageSalariesDefaultviewView.deleteAllVariableEnvironment.refresh}", null},
		{"#{PointageSalariesDefaultviewView.lancerAgent.parameters.lancerAgent.lancerAgent.variablesEnvironnement}", "#{PointageSalariesDefaultviewView.deleteAllVariableEnvironment.result.deleteAllVariableEnvironmentResponse.variablesEnvironnement}"},
		{"#{PointageSalariesDefaultviewView.ajoutVariableToVariablesEnvironnement.parameters.ajoutVariableToVariablesEnvironnement.ajoutVariableToVariablesEnvironnement.variable.name}", "Company"},
		{"#{PointageSalariesDefaultviewView.ajoutVariableToVariablesEnvironnement.parameters.ajoutVariableToVariablesEnvironnement.ajoutVariableToVariablesEnvironnement.variable.value}", "#{PointageSalariesDefaultviewView.company}"},
		{"#{PointageSalariesDefaultviewView.ajoutVariableToVariablesEnvironnement.parameters.ajoutVariableToVariablesEnvironnement.ajoutVariableToVariablesEnvironnement.variablesEnvironnement}", "#{PointageSalariesDefaultviewView.lancerAgent.parameters.lancerAgent.lancerAgent.variablesEnvironnement}"},
		{"#{PointageSalariesDefaultviewView.ajoutVariableToVariablesEnvironnement.refresh}", null},
		{"#{PointageSalariesDefaultviewView.lancerAgent.parameters.lancerAgent.lancerAgent.variablesEnvironnement}", "#{PointageSalariesDefaultviewView.ajoutVariableToVariablesEnvironnement.result.ajoutVariableToVariablesEnvironnementResponse.variablesEnvironnement}"},
		{"#{PointageSalariesDefaultviewView.displayPnl}", "true"},
		{"#{PointageSalariesDefaultviewView.lancerAgent.parameters.lancerAgent.lancerAgent.name}", "PointageGTA"},
		{"#{PointageSalariesDefaultviewView.lancerAgent.parameters.lancerAgent.lancerAgent.type}", "SSIS"},
		{"#{PointageSalariesDefaultviewView.lancerAgent.parameters.lancerAgent.lancerAgent.variablesEnvironnement.folderName}", "InterfaceGTA"},
		{"#{PointageSalariesDefaultviewView.lancerAgent.refresh}", null},
		{"#{PointageSalariesDefaultviewView.lireDerniereInstanceAgent.parameters.lireDerniereInstanceAgent.lireDerniereInstanceAgent.name}", "PointageGTA"},
		{"#{PointageSalariesDefaultviewView.lireDerniereInstanceAgent.refresh}", null},
		{"#{PointageSalariesDefaultviewView.onGiveEtatHistorique}", null},
		{"#{PointageSalariesDefaultviewView.onCheckAvailability}", null},
	};
	/**
	 * Initialize page
	 */
	public String initialize() {
		try {
		    resolveDataBinding(INITIALIZE_PROPERTY_BINDINGS, null, "initialize", true, false);
		} catch (Exception e) {
			error(e);
			log(e);
		}
		return null;	
	}

	public caf.war.RHPortlet.pointagesalaries.PointageSalaries getPointageSalaries()  {
		if (pointageSalaries == null) {
		    pointageSalaries = (caf.war.RHPortlet.pointagesalaries.PointageSalaries)resolveExpression("#{PointageSalaries}");
		}
		return pointageSalaries;
	}

	public java.lang.String getCompany()  {
		
		return company;
	}

	public void setCompany(java.lang.String company)  {
		this.company = company;
	}

	public String onSelectCompany() {
		try{
			resolveDataBinding(ONSELECTCOMPANY_PROPERTY_BINDINGS, this, "onSelectCompany.this", true, false);
			return null;
		} catch(Exception e){}
			return null;
	}

	public java.lang.Boolean getDisplayPnl()  {
		
		return displayPnl;
	}

	
		
	public void setDisplayPnl(java.lang.Boolean displayPnl)  {
		this.displayPnl = displayPnl;
	}

	public java.lang.Boolean getDisplayFire()  {
		
		return displayFire;
	}

	public void setDisplayFire(java.lang.Boolean displayFire)  {
		this.displayFire = displayFire;
	}

	public String onCheckAvailability() {
	    resolveDataBinding(ONCHECKAVAILABILITY_PROPERTY_BINDINGS, this, "onCheckAvailability.this", true, false);
		return null;
	}

	public java.lang.String getUrlFire()  {
		
		return urlFire;
	}

	public void setUrlFire(java.lang.String urlFire)  {
		this.urlFire = urlFire;
	}

	public String onChangeUrl() {
	    resolveDataBinding(ONCHANGEURL_PROPERTY_BINDINGS, this, "onChangeUrl.this", true, false);
	    if (this.getIsRunning().trim().toUpperCase().equals("OUI")){
			this.setUrlFire("/pictures/feuRouge.png");
		} else if (this.getIsRunning().trim().toUpperCase().equals("NON")) {
			this.setUrlFire("/pictures/feuVert.png");
		}
	    return null;
	}

	public java.lang.String getIsRunning()  {
		
		return isRunning;
	}

	public void setIsRunning(java.lang.String isRunning)  {
		this.isRunning = isRunning;
	}

	public java.lang.String getEtatHistorique()  {
		
		return etatHistorique;
	}

	public void setEtatHistorique(java.lang.String etatHistorique)  {
		this.etatHistorique = etatHistorique;
	}

	public String onGiveEtatHistorique() {
	    resolveDataBinding(ONGIVEETATHISTORIQUE_PROPERTY_BINDINGS, this, "onGiveEtatHistorique.this", true, false);
		if (!this.lireDerniereInstanceAgent.getResult().getLireDerniereInstanceAgentResponse().getHisto().getStepId().equals("0")){
			this.setEtatHistorique("En cours");
		} else {
		    if (this.lireDerniereInstanceAgent.getResult().getLireDerniereInstanceAgentResponse().getHisto().getRunStatus().equals("0")){
				this.setEtatHistorique("Echec");
		    } else if (this.lireDerniereInstanceAgent.getResult().getLireDerniereInstanceAgentResponse().getHisto().getRunStatus().equals("1")){
				this.setEtatHistorique("Succ�s");
		    } else if (this.lireDerniereInstanceAgent.getResult().getLireDerniereInstanceAgentResponse().getHisto().getRunStatus().equals("2")){
				this.setEtatHistorique("R�essaie");
		    } else if (this.lireDerniereInstanceAgent.getResult().getLireDerniereInstanceAgentResponse().getHisto().getRunStatus().equals("3")){
				this.setEtatHistorique("Abandonn�");
			}
		}
	    
	    return null;
	}

	public String onRefreshHistorique() {
	    resolveDataBinding(ONREFRESHHISTORIQUE_PROPERTY_BINDINGS, this, "onRefreshHistorique.this", true, false);
		return null;
	}

	public caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsbi.ListeSocieteTimeFit getListeSocieteTimeFit()  {
		if (listeSocieteTimeFit == null) {
		    listeSocieteTimeFit = (caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsbi.ListeSocieteTimeFit)resolveExpression("#{ListeSocieteTimeFit}");
		}
	
	    resolveDataBinding(LISTESOCIETETIMEFIT_PROPERTY_BINDINGS, listeSocieteTimeFit, "listeSocieteTimeFit", false, false);
		return listeSocieteTimeFit;
	}

	public caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.AjoutVariableToVariablesEnvironnement getAjoutVariableToVariablesEnvironnement()  {
		if (ajoutVariableToVariablesEnvironnement == null) {
		    ajoutVariableToVariablesEnvironnement = (caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.AjoutVariableToVariablesEnvironnement)resolveExpression("#{AjoutVariableToVariablesEnvironnement}");
		}
	
	    resolveDataBinding(AJOUTVARIABLETOVARIABLESENVIRONNEMENT_PROPERTY_BINDINGS, ajoutVariableToVariablesEnvironnement, "ajoutVariableToVariablesEnvironnement", false, false);
		return ajoutVariableToVariablesEnvironnement;
	}

	public caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.DeleteAllVariableEnvironment getDeleteAllVariableEnvironment()  {
		if (deleteAllVariableEnvironment == null) {
		    deleteAllVariableEnvironment = (caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.DeleteAllVariableEnvironment)resolveExpression("#{DeleteAllVariableEnvironment}");
		}
	
	    resolveDataBinding(DELETEALLVARIABLEENVIRONMENT_PROPERTY_BINDINGS, deleteAllVariableEnvironment, "deleteAllVariableEnvironment", false, false);
		return deleteAllVariableEnvironment;
	}

	public caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.LancerAgent getLancerAgent()  {
		if (lancerAgent == null) {
		    lancerAgent = (caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.LancerAgent)resolveExpression("#{LancerAgent}");
		}
	
	    resolveDataBinding(LANCERAGENT_PROPERTY_BINDINGS, lancerAgent, "lancerAgent", false, false);
		return lancerAgent;
	}

	public caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.LireDerniereInstanceAgent getLireDerniereInstanceAgent()  {
		if (lireDerniereInstanceAgent == null) {
		    lireDerniereInstanceAgent = (caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.LireDerniereInstanceAgent)resolveExpression("#{LireDerniereInstanceAgent}");
		}
	
	    resolveDataBinding(LIREDERNIEREINSTANCEAGENT_PROPERTY_BINDINGS, lireDerniereInstanceAgent, "lireDerniereInstanceAgent", false, false);
		return lireDerniereInstanceAgent;
	}

	public caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.VerifierJobEnCours getVerifierJobEnCours()  {
		if (verifierJobEnCours == null) {
		    verifierJobEnCours = (caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql.VerifierJobEnCours)resolveExpression("#{VerifierJobEnCours}");
		}
	
	    resolveDataBinding(VERIFIERJOBENCOURS_PROPERTY_BINDINGS, verifierJobEnCours, "verifierJobEnCours", false, false);
		return verifierJobEnCours;
	}

	
}