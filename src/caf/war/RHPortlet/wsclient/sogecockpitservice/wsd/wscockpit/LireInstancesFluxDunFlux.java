package caf.war.RHPortlet.wsclient.sogecockpitservice.wsd.wscockpit;


import java.io.Serializable;

/**
 * Web Service Client bean generated for 
 * fr.sogetrel.srv_bpmsdev.sogecockpitservice_wsd_wscockpit.SOGECockpitServiceWsdWsCockpitStub.lireInstancesFluxDunFlux.
 */
public class LireInstancesFluxDunFlux extends com.webmethods.caf.faces.data.ws.wss.WSSContentProvider {

	private static final long serialVersionUID = 3472361467682952192L;
	
	/**
	 * Constructor
	 */
	public LireInstancesFluxDunFlux() {
		super(fr.sogetrel.srv_bpmsdev.sogecockpitservice_wsd_wscockpit.SOGECockpitServiceWsdWsCockpitStub.class,  // port type proxy class
			"lireInstancesFluxDunFlux", // method to invoke
			new String[] { "lireInstancesFluxDunFlux", } // method parameter names
		);
		
		// init wsclient
		initParams();
		
		
		// parameters bean
		parameters = new Parameters();
			
		// initial result
		result = null;
	}
	
	
	/**
	 * Method parameters bean
	 */
	public class Parameters implements Serializable {

		private static final long serialVersionUID = 1756896398593434624L;
		
		public Parameters() {
		}
	
	  
		private fr.sogetrel.srv_bpmsdev.sogecockpitservice_wsd_wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireInstancesFluxDunFluxE lireInstancesFluxDunFlux  = new  fr.sogetrel.srv_bpmsdev.sogecockpitservice_wsd_wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireInstancesFluxDunFluxE() ;

		public fr.sogetrel.srv_bpmsdev.sogecockpitservice_wsd_wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireInstancesFluxDunFluxE getLireInstancesFluxDunFlux() {
			return lireInstancesFluxDunFlux;
		}

		public void setLireInstancesFluxDunFlux(fr.sogetrel.srv_bpmsdev.sogecockpitservice_wsd_wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireInstancesFluxDunFluxE lireInstancesFluxDunFlux) {
			this.lireInstancesFluxDunFlux = lireInstancesFluxDunFlux;
		}
		
	}
	
	/**
	 * Return method invocation parameters bean
	 */
	public Parameters getParameters() {
		return (Parameters)parameters;
	}	
	


	
	/**
	 * Return method invocation result bean
	 */
	public fr.sogetrel.srv_bpmsdev.sogecockpitservice_wsd_wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireInstancesFluxDunFluxResponseE getResult() {
		return (fr.sogetrel.srv_bpmsdev.sogecockpitservice_wsd_wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireInstancesFluxDunFluxResponseE)result;
	}
	
	
}