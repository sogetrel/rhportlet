package caf.war.RHPortlet.wsclient.sogesectionsaphyrin.wsd.wsinterfacesection;


import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * Web Service Client bean generated for 
 * caf.war.RHPortlet.wsclient.sogesectionsaphyrin.wsd.wsinterfacesection.SOGESectionSAPHYRInWsdWsInterfaceSectionStub.sendSection.
 */
@ManagedBean(name = "SendSection")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(beanType = BeanType.DEFAULT)
public class SendSection extends com.webmethods.caf.faces.data.ws.wss.WSSContentProvider {

	private static final long serialVersionUID = 8838766092159544320L;
	
	/**
	 * Constructor
	 */
	public SendSection() {
		super(caf.war.RHPortlet.wsclient.sogesectionsaphyrin.wsd.wsinterfacesection.SOGESectionSAPHYRInWsdWsInterfaceSectionStub.class,  // port type proxy class
			"sendSection", // method to invoke
			new String[] { "sendSection", } // method parameter names
		);
		
		// init wsclient
		initParams();
		
		
		// parameters bean
		parameters = new Parameters();
			
		// initial result
		result = null;
	}
	
	
	/**
	 * Method parameters bean
	 */
	public class Parameters implements Serializable {

		private static final long serialVersionUID = 8330341617889736704L;
		
		public Parameters() {
		}
	
	  
		private caf.war.RHPortlet.wsclient.sogesectionsaphyrin.wsd.wsinterfacesection.SOGESectionSAPHYRInWsdWsInterfaceSectionStub.SendSectionE sendSection  = new  caf.war.RHPortlet.wsclient.sogesectionsaphyrin.wsd.wsinterfacesection.SOGESectionSAPHYRInWsdWsInterfaceSectionStub.SendSectionE() ;

		public caf.war.RHPortlet.wsclient.sogesectionsaphyrin.wsd.wsinterfacesection.SOGESectionSAPHYRInWsdWsInterfaceSectionStub.SendSectionE getSendSection() {
			return sendSection;
		}

		public void setSendSection(caf.war.RHPortlet.wsclient.sogesectionsaphyrin.wsd.wsinterfacesection.SOGESectionSAPHYRInWsdWsInterfaceSectionStub.SendSectionE sendSection) {
			this.sendSection = sendSection;
		}
		
	}
	
	/**
	 * Return method invocation parameters bean
	 */
	public Parameters getParameters() {
		return (Parameters)parameters;
	}	
	


	
	
}