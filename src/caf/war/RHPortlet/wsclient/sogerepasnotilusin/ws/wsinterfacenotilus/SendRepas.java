package caf.war.RHPortlet.wsclient.sogerepasnotilusin.ws.wsinterfacenotilus;


import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * Web Service Client bean generated for 
 * caf.war.RHPortlet.wsclient.sogerepasnotilusin.ws.wsinterfacenotilus.SOGERepasNotilusInWsWsInterfaceNotilusStub.sendRepas.
 */
@ManagedBean(name = "SendRepas")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(beanType = BeanType.DEFAULT)
public class SendRepas extends com.webmethods.caf.faces.data.ws.wss.WSSContentProvider {

	private static final long serialVersionUID = 7718476731522488320L;
	
	/**
	 * Constructor
	 */
	public SendRepas() {
		super(caf.war.RHPortlet.wsclient.sogerepasnotilusin.ws.wsinterfacenotilus.SOGERepasNotilusInWsWsInterfaceNotilusStub.class,  // port type proxy class
			"sendRepas", // method to invoke
			new String[] { "sendRepas", } // method parameter names
		);
		
		// init wsclient
		initParams();
		
		
		// parameters bean
		parameters = new Parameters();
			
		// initial result
		result = null;
	}
	
	
	/**
	 * Method parameters bean
	 */
	public class Parameters implements Serializable {

		private static final long serialVersionUID = 3380619623799374848L;
		
		public Parameters() {
		}
	
	  
		private caf.war.RHPortlet.wsclient.sogerepasnotilusin.ws.wsinterfacenotilus.SOGERepasNotilusInWsWsInterfaceNotilusStub.SendRepasE sendRepas  = new  caf.war.RHPortlet.wsclient.sogerepasnotilusin.ws.wsinterfacenotilus.SOGERepasNotilusInWsWsInterfaceNotilusStub.SendRepasE() ;

		public caf.war.RHPortlet.wsclient.sogerepasnotilusin.ws.wsinterfacenotilus.SOGERepasNotilusInWsWsInterfaceNotilusStub.SendRepasE getSendRepas() {
			return sendRepas;
		}

		public void setSendRepas(caf.war.RHPortlet.wsclient.sogerepasnotilusin.ws.wsinterfacenotilus.SOGERepasNotilusInWsWsInterfaceNotilusStub.SendRepasE sendRepas) {
			this.sendRepas = sendRepas;
		}
		
	}
	
	/**
	 * Return method invocation parameters bean
	 */
	public Parameters getParameters() {
		return (Parameters)parameters;
	}	
	


	
	
}