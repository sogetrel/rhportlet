package caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsbi;


import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * Web Service Client bean generated for 
 * fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsbi.SOGEBIServiceWsdWsBIStub.listeSocieteTimeFit.
 */
@ManagedBean(name = "ListeSocieteTimeFit2")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(beanType = BeanType.DEFAULT)
public class ListeSocieteTimeFit2 extends com.webmethods.caf.faces.data.ws.wss.WSSContentProvider {

	private static final long serialVersionUID = 2628683993310577664L;
	
	/**
	 * Constructor
	 */
	public ListeSocieteTimeFit2() {
		super(fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsbi.SOGEBIServiceWsdWsBIStub.class,  // port type proxy class
			"listeSocieteTimeFit", // method to invoke
			new String[] { "listeSocieteTimeFit", } // method parameter names
		);
		
		// init wsclient
		initParams();
		
		
		// parameters bean
		parameters = new Parameters();
			
		// initial result
		result = null;
	}
	
	
	/**
	 * Method parameters bean
	 */
	public class Parameters implements Serializable {

		private static final long serialVersionUID = 1616127226080177152L;
		
		public Parameters() {
		}
	
	  
		private fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsbi.SOGEBIServiceWsdWsBIStub.ListeSocieteTimeFitE listeSocieteTimeFit  = new  fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsbi.SOGEBIServiceWsdWsBIStub.ListeSocieteTimeFitE() ;

		public fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsbi.SOGEBIServiceWsdWsBIStub.ListeSocieteTimeFitE getListeSocieteTimeFit() {
			return listeSocieteTimeFit;
		}

		public void setListeSocieteTimeFit(fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsbi.SOGEBIServiceWsdWsBIStub.ListeSocieteTimeFitE listeSocieteTimeFit) {
			this.listeSocieteTimeFit = listeSocieteTimeFit;
		}
		
	}
	
	/**
	 * Return method invocation parameters bean
	 */
	public Parameters getParameters() {
		return (Parameters)parameters;
	}	
	


	
	/**
	 * Return method invocation result bean
	 */
	public fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsbi.SOGEBIServiceWsdWsBIStub.ListeSocieteTimeFitResponseE getResult() {
		return (fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsbi.SOGEBIServiceWsdWsBIStub.ListeSocieteTimeFitResponseE)result;
	}
	
	
}