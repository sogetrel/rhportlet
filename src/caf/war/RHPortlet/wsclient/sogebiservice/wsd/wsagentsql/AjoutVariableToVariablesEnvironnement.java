package caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql;


import java.io.Serializable;

/**
 * Web Service Client bean generated for 
 * fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.ajoutVariableToVariablesEnvironnement.
 */
public class AjoutVariableToVariablesEnvironnement extends com.webmethods.caf.faces.data.ws.wss.WSSContentProvider {

	private static final long serialVersionUID = 8951275344484946944L;
	
	/**
	 * Constructor
	 */
	public AjoutVariableToVariablesEnvironnement() {
		super(fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.class,  // port type proxy class
			"ajoutVariableToVariablesEnvironnement", // method to invoke
			new String[] { "ajoutVariableToVariablesEnvironnement", } // method parameter names
		);
		
		// init wsclient
		initParams();
		
		
		// parameters bean
		parameters = new Parameters();
			
		// initial result
		result = null;
	}
	
	
	/**
	 * Method parameters bean
	 */
	public class Parameters implements Serializable {

		private static final long serialVersionUID = 9191331609816093696L;
		
		public Parameters() {
		}
	
	  
		private fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.AjoutVariableToVariablesEnvironnementE ajoutVariableToVariablesEnvironnement  = new  fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.AjoutVariableToVariablesEnvironnementE() ;

		public fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.AjoutVariableToVariablesEnvironnementE getAjoutVariableToVariablesEnvironnement() {
			return ajoutVariableToVariablesEnvironnement;
		}

		public void setAjoutVariableToVariablesEnvironnement(fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.AjoutVariableToVariablesEnvironnementE ajoutVariableToVariablesEnvironnement) {
			this.ajoutVariableToVariablesEnvironnement = ajoutVariableToVariablesEnvironnement;
		}
		
	}
	
	/**
	 * Return method invocation parameters bean
	 */
	public Parameters getParameters() {
		return (Parameters)parameters;
	}	
	


	
	/**
	 * Return method invocation result bean
	 */
	public fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.AjoutVariableToVariablesEnvironnementResponseE getResult() {
		return (fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.AjoutVariableToVariablesEnvironnementResponseE)result;
	}
	
	
}