package caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql;


import java.io.Serializable;

/**
 * Web Service Client bean generated for 
 * fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.lireDerniereInstanceAgent.
 */
public class LireDerniereInstanceAgent extends com.webmethods.caf.faces.data.ws.wss.WSSContentProvider {

	private static final long serialVersionUID = 9028956050777611264L;
	
	/**
	 * Constructor
	 */
	public LireDerniereInstanceAgent() {
		super(fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.class,  // port type proxy class
			"lireDerniereInstanceAgent", // method to invoke
			new String[] { "lireDerniereInstanceAgent", } // method parameter names
		);
		
		// init wsclient
		initParams();
		
		
		// parameters bean
		parameters = new Parameters();
			
		// initial result
		result = null;
	}
	
	
	/**
	 * Method parameters bean
	 */
	public class Parameters implements Serializable {

		private static final long serialVersionUID = 7384329044901040128L;
		
		public Parameters() {
		}
	
	  
		private fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.LireDerniereInstanceAgentE lireDerniereInstanceAgent  = new  fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.LireDerniereInstanceAgentE() ;

		public fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.LireDerniereInstanceAgentE getLireDerniereInstanceAgent() {
			return lireDerniereInstanceAgent;
		}

		public void setLireDerniereInstanceAgent(fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.LireDerniereInstanceAgentE lireDerniereInstanceAgent) {
			this.lireDerniereInstanceAgent = lireDerniereInstanceAgent;
		}
		
	}
	
	/**
	 * Return method invocation parameters bean
	 */
	public Parameters getParameters() {
		return (Parameters)parameters;
	}	
	


	
	/**
	 * Return method invocation result bean
	 */
	public fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.LireDerniereInstanceAgentResponseE getResult() {
		return (fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.LireDerniereInstanceAgentResponseE)result;
	}
	
	
}