package caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql;


import java.io.Serializable;

/**
 * Web Service Client bean generated for 
 * fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.deleteAllVariableEnvironment.
 */
public class DeleteAllVariableEnvironment extends com.webmethods.caf.faces.data.ws.wss.WSSContentProvider {

	private static final long serialVersionUID = 5623840113309568000L;
	
	/**
	 * Constructor
	 */
	public DeleteAllVariableEnvironment() {
		super(fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.class,  // port type proxy class
			"deleteAllVariableEnvironment", // method to invoke
			new String[] { "deleteAllVariableEnvironment", } // method parameter names
		);
		
		// init wsclient
		initParams();
		
		
		// parameters bean
		parameters = new Parameters();
			
		// initial result
		result = null;
	}
	
	
	/**
	 * Method parameters bean
	 */
	public class Parameters implements Serializable {

		private static final long serialVersionUID = 1835698296193710080L;
		
		public Parameters() {
		}
	
	  
		private fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.DeleteAllVariableEnvironmentE deleteAllVariableEnvironment  = new  fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.DeleteAllVariableEnvironmentE() ;

		public fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.DeleteAllVariableEnvironmentE getDeleteAllVariableEnvironment() {
			return deleteAllVariableEnvironment;
		}

		public void setDeleteAllVariableEnvironment(fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.DeleteAllVariableEnvironmentE deleteAllVariableEnvironment) {
			this.deleteAllVariableEnvironment = deleteAllVariableEnvironment;
		}
		
	}
	
	/**
	 * Return method invocation parameters bean
	 */
	public Parameters getParameters() {
		return (Parameters)parameters;
	}	
	


	
	/**
	 * Return method invocation result bean
	 */
	public fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.DeleteAllVariableEnvironmentResponseE getResult() {
		return (fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.DeleteAllVariableEnvironmentResponseE)result;
	}
	
	
}