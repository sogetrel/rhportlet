package caf.war.RHPortlet.wsclient.sogebiservice.wsd.wsagentsql;


import java.io.Serializable;

/**
 * Web Service Client bean generated for 
 * fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.verifierJobEnCours.
 */
public class VerifierJobEnCours extends com.webmethods.caf.faces.data.ws.wss.WSSContentProvider {

	private static final long serialVersionUID = 3250243000398666752L;
	
	/**
	 * Constructor
	 */
	public VerifierJobEnCours() {
		super(fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.class,  // port type proxy class
			"verifierJobEnCours", // method to invoke
			new String[] { "verifierJobEnCours", } // method parameter names
		);
		
		// init wsclient
		initParams();
		
		
		// parameters bean
		parameters = new Parameters();
			
		// initial result
		result = null;
	}
	
	
	/**
	 * Method parameters bean
	 */
	public class Parameters implements Serializable {

		private static final long serialVersionUID = 2810197117435652096L;
		
		public Parameters() {
		}
	
	  
		private fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.VerifierJobEnCoursE verifierJobEnCours  = new  fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.VerifierJobEnCoursE() ;

		public fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.VerifierJobEnCoursE getVerifierJobEnCours() {
			return verifierJobEnCours;
		}

		public void setVerifierJobEnCours(fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.VerifierJobEnCoursE verifierJobEnCours) {
			this.verifierJobEnCours = verifierJobEnCours;
		}
		
	}
	
	/**
	 * Return method invocation parameters bean
	 */
	public Parameters getParameters() {
		return (Parameters)parameters;
	}	
	


	
	/**
	 * Return method invocation result bean
	 */
	public fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.VerifierJobEnCoursResponseE getResult() {
		return (fr.sogetrel.srv_bpmsdev.sogebiservice_wsd_wsagentsql.SOGEBIServiceWsdWsAgentSqlStub.VerifierJobEnCoursResponseE)result;
	}
	
	
}