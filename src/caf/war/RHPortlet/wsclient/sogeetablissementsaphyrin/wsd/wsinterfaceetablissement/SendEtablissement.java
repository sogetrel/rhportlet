package caf.war.RHPortlet.wsclient.sogeetablissementsaphyrin.wsd.wsinterfaceetablissement;


import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * Web Service Client bean generated for 
 * caf.war.RHPortlet.wsclient.sogeetablissementsaphyrin.wsd.wsinterfaceetablissement.SOGEEtablissementSAPHYRInWsdWsInterfaceEtablissementStub.sendEtablissement.
 */
@ManagedBean(name = "SendEtablissement")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(beanType = BeanType.DEFAULT)
public class SendEtablissement extends com.webmethods.caf.faces.data.ws.wss.WSSContentProvider {

	private static final long serialVersionUID = 2452457259880111104L;
	
	/**
	 * Constructor
	 */
	public SendEtablissement() {
		super(caf.war.RHPortlet.wsclient.sogeetablissementsaphyrin.wsd.wsinterfaceetablissement.SOGEEtablissementSAPHYRInWsdWsInterfaceEtablissementStub.class,  // port type proxy class
			"sendEtablissement", // method to invoke
			new String[] { "sendEtablissement", } // method parameter names
		);
		
		// init wsclient
		initParams();
		
		
		// parameters bean
		parameters = new Parameters();
			
		// initial result
		result = null;
	}
	
	
	/**
	 * Method parameters bean
	 */
	public class Parameters implements Serializable {

		private static final long serialVersionUID = 1661239917189114880L;
		
		public Parameters() {
		}
	
	  
		private caf.war.RHPortlet.wsclient.sogeetablissementsaphyrin.wsd.wsinterfaceetablissement.SOGEEtablissementSAPHYRInWsdWsInterfaceEtablissementStub.SendEtablissementE sendEtablissement  = new  caf.war.RHPortlet.wsclient.sogeetablissementsaphyrin.wsd.wsinterfaceetablissement.SOGEEtablissementSAPHYRInWsdWsInterfaceEtablissementStub.SendEtablissementE() ;

		public caf.war.RHPortlet.wsclient.sogeetablissementsaphyrin.wsd.wsinterfaceetablissement.SOGEEtablissementSAPHYRInWsdWsInterfaceEtablissementStub.SendEtablissementE getSendEtablissement() {
			return sendEtablissement;
		}

		public void setSendEtablissement(caf.war.RHPortlet.wsclient.sogeetablissementsaphyrin.wsd.wsinterfaceetablissement.SOGEEtablissementSAPHYRInWsdWsInterfaceEtablissementStub.SendEtablissementE sendEtablissement) {
			this.sendEtablissement = sendEtablissement;
		}
		
	}
	
	/**
	 * Return method invocation parameters bean
	 */
	public Parameters getParameters() {
		return (Parameters)parameters;
	}	
	


	
	
}