/**
 * 
 */
package caf.war.RHPortlet.interfacesectiontimefit;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * @author vital.thyot
 *
 */

@ManagedBean(name = "InterfaceSectionTimeFitDefaultviewView")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(displayName = "InterfaceSectionTimeFit/default", beanType = BeanType.PAGE)
public class InterfaceSectionTimeFitDefaultviewView  extends   com.webmethods.caf.faces.bean.BasePageBean {

	/**
	 * Determines if a de-serialized file is compatible with this class.
	 *
	 * Maintainers must change this value if and only if the new version
	 * of this class is not compatible with old versions. See Sun docs
	 * for <a href=http://java.sun.com/j2se/1.5.0/docs/guide/serialization/spec/version.html> 
	 * details. </a>
	 */
	private static final long serialVersionUID = 1L;
	private transient caf.war.RHPortlet.interfacesectiontimefit.InterfaceSectionTimeFit interfaceSectionTimeFit = null;
	private transient caf.war.RHPortlet.wsclient.sogesectionsaphyrin.wsd.wsinterfacesection.SendSection sendSection = null;
	private java.lang.Boolean displayInstance;
	private static final String[][] INITIALIZE_PROPERTY_BINDINGS = new String[][] {
		{"#{InterfaceSectionTimeFitDefaultviewView.displayInstance}", "false"},
	};
	private static final String[][] ONLANCER_PROPERTY_BINDINGS = new String[][] {
		{"#{InterfaceSectionTimeFitDefaultviewView.sendSection.refresh}", null},
		{"#{InterfaceSectionTimeFitDefaultviewView.displayInstance}", "true"},
		{"#{InterfaceSectionTimeFitDefaultviewView.lireInstancesFluxDunFlux.parameters.lireInstancesFluxDunFlux.lireInstancesFluxDunFlux.IDDemiFluxIn}", "2"},
		{"#{InterfaceSectionTimeFitDefaultviewView.lireInstancesFluxDunFlux.parameters.lireInstancesFluxDunFlux.lireInstancesFluxDunFlux.IDDemiFluxOut}", "13"},
		{"#{InterfaceSectionTimeFitDefaultviewView.lireInstancesFluxDunFlux.refresh}", null},
	};
	private static final String[][] SENDSECTION_PROPERTY_BINDINGS = new String[][] {
		{"#{sendSection.authCredentials.authenticationMethod}", "1"},
		{"#{sendSection.authCredentials.requiresAuth}", "true"},
		{"#{sendSection.autoRefresh}", "false"},
		{"#{sendSection.endpointAddress}", "#{environment[\"wsclient-endpointAddress-Section\"]}"},
	};
	private transient caf.war.RHPortlet.wsclient.sogecockpitservice.wsd.wscockpit.LireInstancesFluxDunFlux lireInstancesFluxDunFlux = null;
	private static final String[][] LIREINSTANCESFLUXDUNFLUX_PROPERTY_BINDINGS = new String[][] {
		{"#{lireInstancesFluxDunFlux.authCredentials.authenticationMethod}", "1"},
		{"#{lireInstancesFluxDunFlux.authCredentials.requiresAuth}", "true"},
		{"#{lireInstancesFluxDunFlux.autoRefresh}", "false"},
		{"#{lireInstancesFluxDunFlux.endpointAddress}", "#{environment[\"wsclient-endpointAddress-Cockpit\"]}"},
	};
	/**
	 * Initialize page
	 */
	public String initialize() {
		try {
		    resolveDataBinding(INITIALIZE_PROPERTY_BINDINGS, null, "initialize", true, false);
		} catch (Exception e) {
			error(e);
			log(e);
		}
		return null;	
	}

	public caf.war.RHPortlet.interfacesectiontimefit.InterfaceSectionTimeFit getInterfaceSectionTimeFit()  {
		if (interfaceSectionTimeFit == null) {
		    interfaceSectionTimeFit = (caf.war.RHPortlet.interfacesectiontimefit.InterfaceSectionTimeFit)resolveExpression("#{InterfaceSectionTimeFit}");
		}
		return interfaceSectionTimeFit;
	}

	public caf.war.RHPortlet.wsclient.sogesectionsaphyrin.wsd.wsinterfacesection.SendSection getSendSection()  {
		if (sendSection == null) {
		    sendSection = (caf.war.RHPortlet.wsclient.sogesectionsaphyrin.wsd.wsinterfacesection.SendSection)resolveExpression("#{SendSection}");
		}
	
	    resolveDataBinding(SENDSECTION_PROPERTY_BINDINGS, sendSection, "sendSection", false, false);
		return sendSection;
	}

	public java.lang.Boolean getDisplayInstance()  {
		
		return displayInstance;
	}

	public void setDisplayInstance(java.lang.Boolean displayInstance)  {
		this.displayInstance = displayInstance;
	}

	public String onLancer() {
	    resolveDataBinding(ONLANCER_PROPERTY_BINDINGS, this, "onLancer.this", true, false);
		return null;
	}

	public caf.war.RHPortlet.wsclient.sogecockpitservice.wsd.wscockpit.LireInstancesFluxDunFlux getLireInstancesFluxDunFlux()  {
		if (lireInstancesFluxDunFlux == null) {
		    lireInstancesFluxDunFlux = (caf.war.RHPortlet.wsclient.sogecockpitservice.wsd.wscockpit.LireInstancesFluxDunFlux)resolveExpression("#{LireInstancesFluxDunFlux}");
		}
	
	    resolveDataBinding(LIREINSTANCESFLUXDUNFLUX_PROPERTY_BINDINGS, lireInstancesFluxDunFlux, "lireInstancesFluxDunFlux", false, false);
		return lireInstancesFluxDunFlux;
	}
	
}